﻿using UnityEngine;
using System.Collections;

public class ShowInfo : MonoBehaviour {

    //public GameObject quitButton;
    //public GameObject mapButton;
    public GameObject restartButton;
    public GameObject infoLabel;

    private bool toggle = true;
    private JSONObject info;
    void Start() {
        info = ImageSet.getPhoto;
    }

    void OnClick() {
        if (toggle) {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            //Destroy(gameObject.GetComponent<TweenPosition>());
            TweenPosition twp = TweenPosition.Begin(gameObject, 0.15f, new Vector3(gameObject.transform.localPosition.x - 490, gameObject.transform.localPosition.y + 770, 0f));
            twp.RemoveOnFinished(new EventDelegate(IsCollider));
            twp.AddOnFinished(ShowDetail);
        }
        else {

        }
    }
    public void IsCollider() {
        gameObject.GetComponent<BoxCollider>().enabled = true;
    }

    private void ShowDetail() {
        //quitButton.SetActive(true);
        //mapButton.SetActive(true);
        restartButton.SetActive(true);
        infoLabel.SetActive(true);
        infoLabel.GetComponent<UILabel>().text = "Meal : " + info[FoodData.GetFinal()]["meal"] +
            "\n\nRestaurant : " + info[FoodData.GetFinal()]["restaurant"] +
            "\n\n提供者 : " + info[FoodData.GetFinal()]["name"] +
            "\n\n評論 : " + info[FoodData.GetFinal()]["text"];
    }
}
