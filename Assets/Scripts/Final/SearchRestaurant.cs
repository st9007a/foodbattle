﻿using UnityEngine;
using System.Collections;

public class SearchRestaurant : MonoBehaviour {

    void OnClick() {
        LocationInfo coord = Input.location.lastData;
        float la = coord.latitude;
        float lo = coord.longitude;
        //Application.OpenURL("http://maps.google.com/?q=" + la.ToString() + "," + lo.ToString());

        string restaurant = ImageSet.getPhoto[FoodData.GetFinal()]["restaurant"].ToString();
        
        Application.OpenURL("https://www.google.com.tw/maps/search/台南+"+restaurant);
        Application.Quit();


    }
}
