﻿using UnityEngine;
using System.Collections;

public class ShowFood : MonoBehaviour {

    public GameObject mainPanel;
    public GameObject sprite;

	// Use this for initialization
	void Start () {
        
        int num = FoodData.GetFinal();
        gameObject.GetComponent<UITexture>().mainTexture = Resources.Load(FoodData.MainTextureName(num)) as Texture;

        int height = gameObject.GetComponent<UITexture>().mainTexture.height;
        int width = gameObject.GetComponent<UITexture>().mainTexture.width;
        if (width > height)
        {
            gameObject.GetComponent<UIWidget>().height = 1076;
            gameObject.GetComponent<UIWidget>().width = (int)((float)gameObject.GetComponent<UITexture>().mainTexture.width / gameObject.GetComponent<UITexture>().mainTexture.height * 1076);
        }
        else if(width <= height){
            gameObject.GetComponent<UIWidget>().height = (int)((float)gameObject.GetComponent<UITexture>().mainTexture.height / gameObject.GetComponent<UITexture>().mainTexture.width * 1600);
            gameObject.GetComponent<UIWidget>().width = 1600;
        }
        
    }
	
    public void ShowPanel() {
        mainPanel.SetActive(true);
    }
}
