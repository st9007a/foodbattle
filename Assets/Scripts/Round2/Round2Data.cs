﻿using UnityEngine;
using System.Collections;

public class Round2Data : MonoBehaviour {

    public UITexture Right;
    public UITexture Left;
	// Use this for initialization
	void Start () {
        FoodData.Step();

        Texture rightTexture = Resources.Load(FoodData.MainTextureName(FoodData.GetRound2FoodNum(0))) as Texture;
        Texture leftTexture = Resources.Load(FoodData.MainTextureName(FoodData.GetRound2FoodNum(1))) as Texture;

        int rightHeight = rightTexture.height;
        int rightWidth = rightTexture.width;


        if (rightWidth > rightHeight) {
            Right.height = (int)((float)rightHeight / rightWidth * 800);
            Right.width = 800;
        }
        else if (rightWidth == rightHeight)
        {
            Right.height = 800;
            Right.width = 800;
        }
        else {
            Right.height = 1076;
            Right.width = (int)((float)rightWidth / rightHeight * 1076);
        }

        Right.mainTexture = rightTexture;

        int leftHeight = leftTexture.height;
        int leftWidth = leftTexture.width;

        if (leftWidth > leftHeight)
        {
            Left.height = (int)((float)leftHeight / leftWidth * 800);
            Left.width = 800;
        }
        else if (leftWidth == leftHeight) {
            Left.height = 800;
            Left.width = 800;
        }
        else
        {
            Left.height = 1076;
            Left.width = (int)((float)leftWidth / leftHeight * 1076);
        }

        Left.mainTexture = leftTexture;
        
    }
	
}
