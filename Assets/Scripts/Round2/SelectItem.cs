﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectItem : MonoBehaviour {

    public float axisX;
    public GameObject otherSide;
    public GameObject label2;
    public Camera cameraColor;
    public GameObject like;
    public GameObject white;

    private bool backgroundColorChangeToggle = false;
    private int side;

	// Use this for initialization
	void Start () {
        side = (axisX > 0) ? 0 : 1;
    }
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.localPosition = new Vector3(axisX, gameObject.transform.localPosition.y, 0);
        if (backgroundColorChangeToggle)
        {
            //background color control
            cameraColor.backgroundColor = new Color((135.0f - gameObject.transform.localPosition.y / 550 * 90) / 255, 0.5f + gameObject.transform.localPosition.y / 1100, (40.0f + gameObject.transform.localPosition.y / 110) / 255);

            //fb like angle control
            Quaternion likeAngle = new Quaternion();
            float angleDelta = 90 - gameObject.transform.localPosition.y / 550 * 90;
            if (angleDelta > 180)
            {
                angleDelta = 180;
            }
            if (angleDelta < 0)
            {
                angleDelta = 0;
            }

            //fb like position control
            likeAngle.eulerAngles = new Vector3(0, 0, angleDelta);
            like.transform.localRotation = likeAngle;

            float pos = 0f - gameObject.transform.localPosition.y / 550 * 200;
            if (pos > 200)
            {
                pos = 200;
            }
            if (pos < -200)
            {
                pos = -200;
            }
            like.transform.localPosition = new Vector3(axisX, pos, 0);
        }
    }
    void OnDrag()
    {
        backgroundColorChangeToggle = true;
    }
    void OnDragEnd()
    {
        if (gameObject.transform.localPosition.y < 550 && gameObject.transform.localPosition.y > -550)
        {
            TweenPosition tweenPosition = TweenPosition.Begin(gameObject, 0.05f, new Vector3(axisX, 0, 0));
            tweenPosition.AddOnFinished(DragEnd);
        }
        else 
        {
            backgroundColorChangeToggle = false;

            if (gameObject.transform.localPosition.y >= 550) {
                FoodData.SelectFoodAtRound2(side);

            }
            else {
                FoodData.SelectFoodAtRound2((side == 1) ? 0 : 1);
            }

            //FoodData.SelectFoodAtRound2(side);
            bool check = FoodData.CheckRound2();   //check if all food images have been selected

            if (check == false)
            {
                FoodData.Step();

                ChangeTexture();

                gameObject.transform.localPosition = new Vector3(axisX, 0, 0);

                //animate
                TweenAlpha tweenAlpha = TweenAlpha.Begin(white, 0.05f, 1);
                tweenAlpha.AddOnFinished(SelectedAnim);

            }
            else
            {
                //cutscence 
                white.GetComponent<UISprite>().alpha = 1;

                otherSide.SetActive(false);
                TweenPosition.Begin(label2, 0.5f, new Vector3(-600, 0, 0));
                /////////////////////////////////////////////////////////////////

                Invoke("LoadScene", 0.5f);
                FoodData.SetFinal();


                UIWidget thisTexture = gameObject.GetComponent<UIWidget>();
                thisTexture.depth = -1;
            }
        }
        
    }

    void ChangeTexture() {


        int num = FoodData.GetRound2FoodNum(side);
        Texture mainTexture = Resources.Load(FoodData.MainTextureName(num)) as Texture;

        num = FoodData.GetRound2FoodNum((side == 1) ? 0 : 1);
        Texture otherSideTexture = Resources.Load(FoodData.MainTextureName(num)) as Texture;

        int mainHeight = mainTexture.height;
        int mainWidth = mainTexture.width;


        if (mainWidth > mainHeight)
        {
            gameObject.GetComponent<UITexture>().height = (int)((float)mainHeight / mainWidth * 800);
            gameObject.GetComponent<UITexture>().width = 800;
        }
        else if (mainWidth == mainHeight)
        {
            gameObject.GetComponent<UITexture>().height = 800;
            gameObject.GetComponent<UITexture>().width = 800;
        }
        else
        {
            gameObject.GetComponent<UITexture>().height = 1076;
            gameObject.GetComponent<UITexture>().width = (int)((float)mainWidth / mainHeight * 1076);
        }

        gameObject.GetComponent<UITexture>().mainTexture = mainTexture;

        int otherSideHeight = otherSideTexture.height;
        int otherSideWidth = otherSideTexture.width;

        if (otherSideWidth > otherSideHeight)
        {
            otherSide.GetComponent<UITexture>().height = (int)((float)otherSideHeight / otherSideWidth * 800);
            otherSide.GetComponent<UITexture>().width = 800;
        }
        else if (otherSideWidth == otherSideHeight)
        {
            otherSide.GetComponent<UITexture>().height = 800;
            otherSide.GetComponent<UITexture>().width = 800;
        }
        else
        {
            otherSide.GetComponent<UITexture>().height = 1076;
            otherSide.GetComponent<UITexture>().width = (int)((float)otherSideWidth / otherSideHeight * 1076);
        }

        otherSide.GetComponent<UITexture>().mainTexture = otherSideTexture;
    }


    private void LoadScene()
    {
        SceneManager.LoadScene("_FINAL");
    }
    private void DragEnd()
    {
        backgroundColorChangeToggle = false;
        Destroy(gameObject.GetComponent("TweenPosition"));
    }
    private void SelectedAnim()
    {
        gameObject.transform.localPosition = new Vector3(axisX, -800, 0);
        otherSide.transform.localPosition = new Vector3(0 - axisX, -800, 0);
        
        TweenPosition.Begin(otherSide, 0.15f, new Vector3(0 - axisX, 0, 0));
        TweenPosition tweenPosition = TweenPosition.Begin(gameObject, 0.15f, new Vector3(axisX, 0, 0));

        tweenPosition.AddOnFinished(whiteAlphaToZero);
    }
    private void whiteAlphaToZero()
    {
        UISprite whiteAlpha = white.GetComponent<UISprite>();
        whiteAlpha.alpha = 0;
    }
    
}
