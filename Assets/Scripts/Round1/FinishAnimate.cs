﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinishAnimate : MonoBehaviour {

    public string scene;
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.localPosition.y == 0) {
            SceneManager.LoadScene(scene);
        }
    }
}
