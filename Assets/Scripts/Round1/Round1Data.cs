﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Round1Data : MonoBehaviour {

	public UILabel numBeSelected;
    public UILabel upBound;
    public UILabel mode;
	public UITexture foodSprite_1;
	public GameObject finish;
	public GameObject overlay;
	private static int count;

	// Use this for initialization
	void Start () {
        //int upBound = ImageSet.getPhoto.Count;
        /*FoodData.Initial();
		FoodData.Rand();*/

        count = 0;
        foodSprite_1.mainTexture = Resources.Load(FoodData.MainTextureName(FoodData.GetFoodNum(0))) as Texture; 

        upBound.text = "/" + FoodData.UpBound;

        if (FoodData.Round1Mode == FoodData.Mode.Select) mode.text = "(Select)";
        else mode.text = "(Watch)";
	}
	
	// Update is called once per frame
	void Update () {
        //顯示已經選了幾個
        numBeSelected.text = count.ToString();

        //結束條件
        int foodLength = FoodData.GetFoodLength();
        if (count == FoodData.UpBound || foodLength == 0)
        {
            
            overlay.SetActive(true);
            Animator anim = finish.GetComponent<Animator>();
            anim.SetTrigger("play");

        }
    }
    public static void AddCount()
    {
        count++;
    }
}
