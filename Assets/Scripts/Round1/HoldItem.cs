﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HoldItem : MonoBehaviour {

	public Camera cameraColor;
	public GameObject like;
    public GameObject foodImage;

	void Start () {
		BoxCollider  box =  gameObject.GetComponent<BoxCollider >();
		box.enabled = true;
		cameraColor.backgroundColor = new Color (1f, 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		//make object move vertically only
		gameObject.transform.localPosition = new Vector3(0f,gameObject.transform.localPosition.y,0f);

		//cameraColor.backgroundColor = new Color(255/255,45.0/255.0,45.0/255.0);  red
		//cameraColor.backgroundColor = new Color(45.0/255,1,55.0/255);            green

		//background color control
		cameraColor.backgroundColor = new Color((135.0f-gameObject.transform.localPosition.y/550*90)/255,0.5f+gameObject.transform.localPosition.y/1100,(40.0f+gameObject.transform.localPosition.y/110)/255);

		//fb like angle control
		Quaternion likeAngle = new Quaternion();
		float angleDelta = 90f - gameObject.transform.localPosition.y / 550 * 90;
		if(angleDelta > 180f){
			angleDelta = 180f;
		}
		if(angleDelta < 0f){
			angleDelta = 0f;
		}

		//fb like position control
		likeAngle.eulerAngles = new Vector3(0f,0f,angleDelta);
		like.transform.localRotation = likeAngle;

		float pos = 0 - gameObject.transform.localPosition.y / 550 * 200;
		if(pos > 200f){
			pos = 200f;
		}
		if(pos < -200f){
			pos = -200f;
		}
		like.transform.localPosition = new Vector3 (0, pos, 0);


        int height = foodImage.GetComponent<UITexture>().mainTexture.height;
        int width = foodImage.GetComponent<UITexture>().mainTexture.width;

        if (width > height)
        {
            foodImage.GetComponent<UIWidget>().height = 1076;
            foodImage.GetComponent<UIWidget>().width = (int)((float)foodImage.GetComponent<UITexture>().mainTexture.width / foodImage.GetComponent<UITexture>().mainTexture.height * 1076);
        }
        else if (width <= height)
        {
            foodImage.GetComponent<UIWidget>().height = (int)((float)foodImage.GetComponent<UITexture>().mainTexture.height / foodImage.GetComponent<UITexture>().mainTexture.width * 1600);
            foodImage.GetComponent<UIWidget>().width = 1600;
        }
    }

    void OnDragEnd()
    {

        if (gameObject.transform.localPosition.y < 550 && gameObject.transform.localPosition.y > -550)
        {
            TweenPosition.Begin(gameObject, 0.05f, new Vector3(0, 0, 0));
        }
        else
        {

            int foodLength;
            
            if (gameObject.transform.localPosition.y >= 550)
            {

                //store the data
                Round1Data.AddCount();

                //store the index of the food player select
                FoodData.SelectFoodAtRound1();

                //reset the game object to next food
                gameObject.transform.localPosition = new Vector3(0, 0, 0);

                //shift the selected food data 
                FoodData.Shift();
                foodLength = FoodData.GetFoodLength();

                //set new image to gameObject
                foodImage.GetComponent< UITexture>().mainTexture = Resources.Load(FoodData.MainTextureName(FoodData.GetFoodNum(0)))as Texture;
            }

            if (gameObject.transform.localPosition.y <= -550)
            {

                if (FoodData.Round1Mode == FoodData.Mode.Watch) {
                    Round1Data.AddCount();
                }

                gameObject.transform.localPosition = new Vector3(0, 0, 0);

                FoodData.Shift();

                foodLength = FoodData.GetFoodLength();

                if (foodLength == 0)
                {
                    Destroy(like);
                    Destroy(gameObject);
                }
                else
                {
                    foodImage.GetComponent<UITexture>().mainTexture = Resources.Load(FoodData.MainTextureName(FoodData.GetFoodNum(0))) as Texture;
                    
                }
            }
        }
    }
}
