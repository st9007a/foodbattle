﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ImageSet : MonoBehaviour {
	
	private WWW data;
	static public JSONObject getPhoto;

	public GameObject load;
    public GameObject title;
    public float waitConnectTime = 10f; 

	IEnumerator getData(){

        load.SetActive (true);

        do
        {
            data = new WWW("http://merry.ee.ncku.edu.tw:9000/get_photo");

            yield return data;

            if (data.text != "")
            {
                getPhoto = new JSONObject(data.text);

                FoodData.Length = getPhoto.Count - 1 > 68 ? 68 : getPhoto.Count - 1;
                load.SetActive(false);
                Debug.Log("get data");

                title.SetActive(true);
                
                break;
            }
            else
            {
                Debug.Log("connect error");                
            }

        } while (data.text == "");

        
	}

    void Start() {
        StartCoroutine(getData());
    }
    void Update() {
        waitConnectTime -= Time.deltaTime;
        if (waitConnectTime <= 0) {
            load.transform.GetChild(0).gameObject.GetComponent<UILabel>().text = "Connect Error\nPlease check your network";
            if (data.isDone && data.text != "") {
                load.transform.GetChild(0).gameObject.GetComponent<UILabel>().text = "Loading ...";
            }
        }
    }
    
}
