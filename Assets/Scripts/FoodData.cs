﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class FoodData  {

    public enum Mode {
        Select,
        Watch
    }

    private static int length;
    private static int upBound;

    private static Mode round1Mode = Mode.Select;

	private static List<int> foodNum;
	private static List<int> round1Food;
	private static List<int> round2Food;

	private static int finalFood;

    public static int Length {
        set { length = value; }
        get { return length; }
    }
    public static int UpBound {
        set { upBound = value; }
        get { return upBound; }
    }
    public static Mode Round1Mode {
        set { round1Mode = value; }
        get { return round1Mode; }
    }

	public static void Initial(){

		foodNum = new List<int>();
		round1Food = new List<int>();
		round2Food = new List<int>();

		for(int i=1;i<=length;i++){
            foodNum.Add(i);
		}
	}
	//////////////////////////////////////////////////////////////
	//use to round 1
	public static void Rand(){
		for(int i=0;i<foodNum.Count;i++){
            int rand = Random.Range(0, foodNum.Count);
			int clone = foodNum[i];
			foodNum[i] = foodNum[rand];
			foodNum[rand] = clone;
		}
        string str = "";
        for (int i = 0; i < foodNum.Count; i++) str += foodNum[i] + " ";

        //Debug.Log(str);
        //Debug.Log(foodNum.Count);
	}

	public static void SelectFoodAtRound1(){
		if(round1Food.Count <= upBound){
			//store the data that user select at  round 1
			round1Food.Add(foodNum[0]);

        }
	}

	public static void Shift(){
		foodNum.RemoveAt(0);
    }

	public static int GetFoodNum(int i){
		return foodNum[i];
	}

	public static int GetFoodLength(){
		return foodNum.Count;
	}
	////////////////////////////////////////////////////////
	//use to round 2
	public static void Step(){
		round2Food.Add(round1Food[0]);
		round2Food.Add(round1Food[1]);
		round1Food.RemoveAt(0);
		round1Food.RemoveAt(0);
	}

	public static int GetRound2FoodNum(int index){
		if(round2Food.Count <= 2){
			return round2Food[index];
		}
		else{
			return -1;
		}
	} 

	public static void SelectFoodAtRound2(int index){
		round1Food.Add(round2Food[index]);
		round2Food = new List<int>();
	}

	public static bool CheckRound2(){
		if(round1Food.Count == 1){
			return true;
		}
		else{
			return false;
		}
	}

	public static void SetFinal(){
		int num = round1Food[0];
		finalFood = num;
	}
	///////////////////////////////////////////////////////
	//use to final
	public static int GetFinal(){
		if(round1Food.Count == 1){
			return finalFood;
		}
		else{
			return 1;
		}
	}
    /// //////////////////////////////////////////////////

    //return image path at resourcs folder
    public static string MainTextureName(int i)
    {
        string imagePath = ImageSet.getPhoto[i]["filename"].ToString();

        imagePath = imagePath.Remove(imagePath.Length - 1, 1);
        imagePath = imagePath.Remove(0, 1);
        imagePath = "FoodImage/" + imagePath;

        string jpg = imagePath;
        jpg = jpg.Remove(jpg.Length - 4, 4);

        string jpeg = imagePath;
        jpeg = jpeg.Remove(jpeg.Length - 5, 5);


        imagePath = Resources.Load(imagePath.Remove(imagePath.Length - 4, 4)) == new Object() ? jpeg : jpg;

        return imagePath;
    }
}
