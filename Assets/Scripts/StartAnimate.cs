﻿using UnityEngine;
using System.Collections;

public class StartAnimate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.localPosition = Vector3.MoveTowards(gameObject.transform.localPosition, new Vector3(0f, 1750f, 0f), Time.deltaTime * 1500);
        if (gameObject.transform.localPosition == new Vector3(0f, 1750f, 0f))  {
            gameObject.transform.parent.gameObject.SetActive(false);
        }
    }
}
