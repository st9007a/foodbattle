﻿using UnityEngine;
using System.Collections;

public class AnimateForStart : MonoBehaviour {

    public UILabel title;
    public UITexture background;
    public GameObject startButton;
    public GameObject quitButton;
    public GameObject settingButton;


	void Update () {
        if (trigger && title.spacingX > 0) {
            title.spacingX -= 10;
            if (title.spacingX == 0) {
                secondTrigger = true;
                trigger = false;
            }
        }

        if (secondTrigger && background.fillAmount != 1f) {
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                title.gameObject.SetActive(false);
                background.fillAmount += 0.05f;
                if (background.fillAmount == 1) {
                    secondTrigger = false;
                }
            }
        }

        if (background.fillAmount == 1 && startButton.transform.localPosition.y == 1000) {
            TweenPosition tweenStart = TweenPosition.Begin(startButton, 0.7f, new Vector3(0, 123, 0));
            TweenPosition tweenQuit = TweenPosition.Begin(quitButton, 0.7f, new Vector3(0, -335, 0));
            TweenPosition tweenSetting = TweenPosition.Begin(settingButton, 0.7f, new Vector3(600, -335, 0));
            tweenStart.method = UITweener.Method.BounceIn;
            tweenQuit.method = UITweener.Method.BounceIn;
            tweenSetting.method = UITweener.Method.BounceIn;
        }
	}

    bool trigger = false;
    bool secondTrigger = false;
    float delay = 0.3f;

    public void TitleSpaceX() {
        trigger = true;
    }
}
