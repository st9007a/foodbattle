﻿using UnityEngine;
using System.Collections;

public class SettingOptionButton : MonoBehaviour {

    public GameObject settingButton;
    public int id;
    public FoodData.Mode mode;
    public int value;

    void OnClick() {
        settingButton.GetComponent<Setting>().Click(id);
        FoodData.UpBound = value;
        FoodData.Round1Mode = mode;
    }
}
