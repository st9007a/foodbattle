﻿using UnityEngine;
using System.Collections;

public class Setting : MonoBehaviour {

    public GameObject circle;
    public GameObject buttonSprite;      //sprite of setting button 
    public UILabel buttonText;           //label of setting button
    public GameObject buttonStart;       //start button
    public GameObject buttonQuit;        //quit button
    public GameObject settingPanel;      //setting panel
    public GameObject[] buttonList;      //select and watch button

    private bool[] click;
    private bool toggle = false;

    void Start() {
        click = new bool[buttonList.Length];
        for (int i = 0; i < click.Length; i++) click[i] = false;

        click[0] = true;
        buttonList[0].GetComponent<UISprite>().alpha = 1;
        FoodData.UpBound = buttonList[0].GetComponent<SettingOptionButton>().value;
    }

    void OnClick()
    {

        TweenScale tweenScale;
        if (!toggle)
        {
            circle.SetActive(false);
            buttonSprite.SetActive(true);
            //tween : change scale of panel 
            TweenScale.Begin(buttonSprite, 0.2f, new Vector3(7, 10, 0));

            //setting button change to ok button
            buttonText.text = "OK";

            //add event "show setting panel" to tween onfinished
            tweenScale = buttonSprite.GetComponent<TweenScale>();
            tweenScale.AddOnFinished(showSettingPanel);

            //hide start and quit button
            buttonStart.SetActive(false);
            buttonQuit.SetActive(false);

        }
        else
        {
            //tween : change scale of panel 
            TweenScale.Begin(buttonSprite, 0.2f, new Vector3(1, 1, 0));

            //ok button change to setting button
            buttonText.text = "Settings";

            //add event "hide setting panel" to tween onfinished
            tweenScale = buttonSprite.GetComponent<TweenScale>();
            tweenScale.AddOnFinished(disSettingPanel);

            //hide setting panel
            settingPanel.SetActive(false);
        }

        toggle = !toggle;
    }
    private void showSettingPanel()
    {

        //show setting panel
        settingPanel.SetActive(true);

        //remove tween scale
        Destroy(buttonSprite.GetComponent<TweenScale>());
    }
    private void disSettingPanel()
    {

        //hide setting panel
        settingPanel.SetActive(false);

        //show start and quit button
        buttonStart.SetActive(true);
        buttonQuit.SetActive(true);

        //
        circle.SetActive(true);
        buttonSprite.SetActive(false);

        //remove tween scale
        Destroy(buttonSprite.GetComponent<TweenScale>());


    }

    public void Click(int id) {
        int index = -1;
        for (int i = 0; i < click.Length; i++) {
            if (click[i] == true) {

                buttonList[i].GetComponent<UISprite>().alpha = 115f / 255f;
                click[i] = false;
                click[id] = true;
                index = id;
                break;
            }
        }

        if (index == -1) click[id] = true;

        buttonList[id].GetComponent<UISprite>().alpha = 1;
    }
}
