﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartBtn : MonoBehaviour {

    public string sceneName;
    public GameObject quitButton;
    public GameObject settingButton;
    public UITexture background;
    public GameObject startButtonLabel;

    bool trigger_1 = false;
    bool trigger_2 = false;

    void OnClick() {

        FoodData.Initial();
        FoodData.Rand();

        TweenScale tweenScale = transform.parent.gameObject.GetComponent<TweenScale>();
        tweenScale.PlayForward();   
    }
    public void ScaleChange() {
        quitButton.SetActive(false);
        settingButton.SetActive(false);
        TweenScale tweenScale = TweenScale.Begin(gameObject, 0.3f, new Vector3(6, 6, 1));
        tweenScale.AddOnFinished(() => { trigger_1 = true; });
    }



    void Update() {
        if (trigger_1) {
            background.fillAmount -= 0.05f;
            if (background.fillAmount == 0) {
                trigger_1 = false;
                background.alpha = 1;
                Texture newBackground = Resources.Load(FoodData.MainTextureName(FoodData.GetFoodNum(0))) as Texture;

                int height = newBackground.height;
                int width = newBackground.width;

                if (width > height)
                {
                    background.height = 1076;
                    background.width = (int)((float)width / height * 1076);
                }
                else if (width <= height)
                {
                    background.height = (int)((float)height / width * 1600);
                    background.width = 1600;
                }

                background.mainTexture = newBackground;

                trigger_2 = true;
                
            }
        }
        if (trigger_2) {
            background.fillAmount += 0.05f;
            gameObject.GetComponent<UISprite>().fillAmount -= 0.05f;
            if (background.fillAmount == 1)
            {
                trigger_2 = false;
                startButtonLabel.GetComponent<TweenScale>().PlayForward();
                startButtonLabel.GetComponent<TweenRotation>().PlayForward();
                startButtonLabel.GetComponent<TweenColor>().PlayForward();
            }
        }
    }

    public void LoadScene() {
        SceneManager.LoadScene(sceneName);
    }
}
