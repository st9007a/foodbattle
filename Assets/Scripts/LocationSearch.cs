﻿using UnityEngine;
using System.Collections;

public class LocationSearch : MonoBehaviour {

    IEnumerator Start()
    {

        if (!Input.location.isEnabledByUser)
        {
            yield break;
        }

        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {

            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {

            yield break;
        }



    }

    // Update is called once per frame
    void Update () {
	
	}
}
